import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import Router from 'next/router';
import Form from './styles/Form';
import formatMoney from  '../lib/formatMoney';
import Error from './ErrorMessage';
import fetch from 'node-fetch';

const CREATE_ITEM_MUTATION = gql`
  mutation CREATE_ITEM_MUTATION (
      $title: String!
      $description: String!
      $price: Int!
      $image: String
      $largeImage:  String
  ){
    createItem( 
      title: $title
      description: $description
      price: $price
      image: $image
      largeImage: $largeImage
    ){
      id
    }
  }
`;

class CreateItem extends Component {
  state = {
    title: 'Coolest shoes',
    description: 'I love those context',
    image: '',
    largeImage: '',
    price: 1000
  }

  handleChange = (e) => {
    const { name, type, value } = e.target;
    const val = type === 'number' ? parseFloat(value) : value;
    this.setState({ [name]: val });
  }

   uploadFile = async e => {
    console.log("Uploading file");
    const files = e.target.files;
    const data = new FormData();
    data.append('file', files[0]);
    data.append('upload_preset',  'sickfits');

    const res = await fetch(
      'https://api.cloudinary.com/v1_1/julianalvarez/image/upload ', 
      {
        method: 'POST',
        body: data
      }
    );

    const file = await res.json();
    console.log(file); 
    this.setState({
      image: file.secure_url,
      largeImage: file.eager[0].secure_url
    });
  
  }

  render() {
    return (
      <Mutation mutation={CREATE_ITEM_MUTATION} variables={this.state}>
        {(createItem, { loading, error }) => (
          <Form onSubmit={ async e => {
            // Stop the form from submitting
            e.preventDefault();
            // call the mutation
            const res = await createItem();
            // Change them to the single item page
            console.log(res);
            Router.push({
              pathname: '/item',
              query: { id: res.data.createItem.id }
            });
          }}>

            <Error error={error} />

            <fieldset disabled={loading} aria-busy={loading}>
              <label htmlFor="file">
                Image
                <input 
                  type="file" 
                  name="file"
                  id="title" 
                  placeholder="Upload an image" 
                  required 
                  onChange={this.uploadFile }
                />
                {this.state.image && <img width="200" src={this.state.image} alt="Upload preview " /> }
              </label>

              <label htmlFor="title">
                Title
                <input 
                  type="text" 
                  name="title"
                  value={this.state.title} 
                  id="title" 
                  placeholder="title" 
                  required 
                  onChange={this.handleChange }
                />
              </label>

              <label htmlFor="price">
                Price
                <input 
                  type="number" 
                  name="price"
                  value={this.state.price} 
                  id="price" 
                  placeholder="Price" 
                  required 
                  onChange={this.handleChange }
                />
              </label>

              <label htmlFor="description">
                Description
                <textarea 
                  name="description"
                  value={this.state.description} 
                  id="description" 
                  placeholder="description" 
                  required 
                  onChange={this.handleChange }
                />
              </label>

              <button>Submit</button>
            </fieldset> 
          </Form>
        )}
      </Mutation>
    );
  }
}

export default CreateItem;
export { CREATE_ITEM_MUTATION };