import react from 'react';
import StripeCheckout from 'react-stripe-checkout';
import { Mutation } from 'react-apollo';
import Router from 'next/router';
import Nprogress from 'nprogress';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import calcTotalPrice from '../lib/calcTotalPrice';
import Error from './ErrorMessage';
import User, { CURRENT_USER_QUERY } from './User';

const CREATE_ORDER_MUTATION = gql`
  mutation createOrder($token: String!) {
    createOrder(token: $token) {
      id
      charge
      total
      items {
        id
        title
      }
    }
  }
`;


function totalItems(cart) {
  return cart.reduce((tally, cartItem) => tally + cartItem.quantity, 0);
}

class TakeMyMoney extends React.Component {
  onToken = async (res, createOrder) => {
    Nprogress.start();
    console.log('On token called!');
    console.log(res.id);
    // Manually call the mutation once we have the stripe token
    const order = await createOrder({
      variables: {
        token: res.id
      },
    }).catch(err => {
      alert(err.message);
    });
    // redirect to the order created
    Router.push({
      pathname: '/order',
      query: { id: order.data.createOrder.id },
    });


  };
  render() {
  return (
    <User>
      {({ data: { me }, loading }) => {
        
        if (loading) return null;
        return (
          <Mutation 
            mutation={CREATE_ORDER_MUTATION}
            refetchQueries={[ { query: CURRENT_USER_QUERY } ]}
          >
            {(createOrder) => (
              <StripeCheckout
                amount={me.cart && calcTotalPrice(me.cart)}
                name={"Sick Fits"}
                description={`Order of ${totalItems(me.cart)} items!`}
                image={me.cart.length && me.cart[0].item && me.cart[0].item.image}
                stripeKey="pk_test_ahMgtFw68YDF9EneyO7a5dYV"
                currency="USD"
                email={me.email}
                token={res => this.onToken(res, createOrder)}
              >
                {this.props.children}
              </StripeCheckout>
            )}
          </Mutation>
        )
      }}
    </User>
  );
  }
}

export default TakeMyMoney;